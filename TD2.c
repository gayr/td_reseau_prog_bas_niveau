#include <stdio.h>
#include<stdlib.h>

//Question 1
int maximum(int *t, int n){
int max = t[0];
for(int i = 1; i< n; i++){
    if(max< t[i]){
        max = t[i];
    }
}
return max;
}

void f(int a, int b, int *s, int *p) {
    *s = a + b;
    *p = a * b;
}
void minmax(int *t, int n, int *pmin, int *pmax){
    *pmax = t[0];
    *pmin = t[0];
    for(int i =1; i<n;i++){
    if(*pmax < t[i]){
        *pmax = t[i];
    }else if(*pmin > t[i]){
        *pmin = t[i];
    }
    }
}
int* copie(int *tab, int n) {
    int tab2[n];
    for (int i = 0; i < n; i++) {
        tab2[i] = tab[i];
    }
    return tab2;
}

int* newcopie(int *tab, int n) {
    int *tab3 = malloc(sizeof(int) * n);
    for (int i = 0; i < n; i++) {
        tab3[i] = tab[i];
    }
    return tab3;
}

int* unsurdeux(int *tab, int n) {
    int *tab4 = malloc(sizeof(int) * n / 2);
    int j= 0;
    for (int i = 0; i < n - 1; i += 2) {
        tab4[j] = tab[i];
        j++;
    }
    return tab4;
}



void Mainquestion1(){
int tab[5] = {3,4,7,1,9};
printf("%i\n",maximum(tab, 5));
}

void Mainquestion2(){
int x, y;
f(12, 4, &x, &y);
printf("x = %d, y = %d\n", x, y);
}
void Mainquestion4_5(){
    int tab[10] ={2,3,8,1,4,9,3,0,8,4};
    int x;
     int y;
    minmax(tab, 10, &x, &y);
    printf("x = %i, y = %i \n", x, y);
}
void Mainquestion6(){
    int tab[5] = {3,4,7,1,9};
    copie(tab, 5);
    
}
void Mainquestion7(){
    int tab2[5] = {3,4,7,1,9};
    newcopie(tab2, 5);
    
}
void Mainquestion8(){
    int tab[] = {1, 2, 3, 4, 5, 6, 7, 8, 9};
    int *tab_bis;
    
    tab_bis = unsurdeux(tab, 10);
    for (int i = 0; i < 5; i++) {
        printf("%d ", tab_bis[i]);
    }
    printf("\n");
    free(tab_bis);

    
}

int main(){
   /* Mainquestion1();
    Mainquestion2();
    Mainquestion4_5();
    Mainquestion6();
    Mainquestion7();*/
    Mainquestion8();
}


// Question 2 : On doit donner la taille du tableau en paramètre de la fonction parce qu'il est impossible de connaitre la taille du tableau autrement.

// Question 3 : Lorsque l'on execute le code suivant, on donne en paramètre les adresses des variables x et y, puis la fonction f met les valeurs dans &x et &y, l'opération se fait et se stock dans les adresses données en paramtère.

// Question 6 : L'adresse mémoire allouée de tab2 disparait à la fin de la fonction.

